﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaxiBooking.Models
{
    public class Booking
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string PickUpPoint { get; set; }
        public string Destination { get; set; }
        public double CurrentLatitude { get; set; }
        public double CurrentLongitude { get; set; }


    }
}
